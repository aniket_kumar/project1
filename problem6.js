/**
 * Generate array of only Audi and BMW cars,and return it in JSON format
 * @param {*} inventory -Inventory array
 */
function getAudiBMW(inventory) {
    let AudiBMWArray=[];
    if(Array.isArray(inventory) && inventory.length > 0){
        for(let i=0;i<inventory.length;i++){
            if(inventory[i].car_make === 'Audi' || inventory[i].car_make ==='BMW'){
                AudiBMWArray.push(inventory[i]);
            }
        }
        return AudiBMWArray;
    }else{
        return AudiBMWArray;
    }
}
module.exports = getAudiBMW;