/**
 * Generate count of car which are manufactured after 2000
 * @param {*} inventory -Inventory array
 */
function countCars(inventory) {
    let res=[];
    if(Array.isArray(inventory) && inventory.length>0){
        for(let i=0;i<inventory.length;i++){
            if(inventory[i].car_year > 2000) res.push(inventory[i]);
        }
        return res;
    }else{
        return res;
    }
}
module.exports =countCars;