/**
 * Returns details of last car in Inventory
 * @param {*} inventory - Inventory array
 */
function lastCar(inventory){
    let res=[];
    if(Array.isArray(inventory) && inventory.length>0){
        const length=inventory.length;
        res.push(`Last car is a ${inventory[length-1].car_make} ${inventory[length-1].car_model}`);
        return res;
    }else{
        return res;
    }
}

module.exports =lastCar;