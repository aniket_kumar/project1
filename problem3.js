/**
 * Generate array in sorted order of car_model alphabetical order
 * @param {*} inventory -Inventory array
 */
function sortAlphabetically(inventory){
    if(Array.isArray(inventory) && inventory.length>0){
        const sortedArr=inventory.sort(function (a,b) {
        if(a.car_model.toLowerCase() < b.car_model.toLowerCase()) return -1;
        if(a.car_model.toLowerCase() > b.car_model.toLowerCase()) return 1;
        return 0;
        });
        return sortedArr;
    }else{
        let res=[];
        return res;
    }
}

module.exports = sortAlphabetically;