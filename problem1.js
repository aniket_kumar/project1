/**
 * Returns resArray details of given id
 * @param {*} inventory - Inventory array
 * @param {*} id - id of car to be extracted
 */
function DetailsById(inventory,id){
    let res=[];
    if(id == null){
        return res;
    }
    if(Array.isArray(inventory) && inventory.length>0){
        for(let i=0;i<inventory.length;i++){
            if(inventory[i].id === id){
                const str=`Car ${inventory[i].id} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`;
                res.push(str);
                return res;
            }
        }
    }else{
        return res;
    }
}

module.exports =DetailsById;