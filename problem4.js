/**
 * Generates array consists of car_year
 * @param {*} inventory - Inventory array
 */
function getDate(inventory) {
    const dateArray=[];
    if(Array.isArray(inventory) && inventory.length>0){
        for(let i=0;i<inventory.length;i++){
            dateArray.push(inventory[i].car_year);
        }
        return dateArray;
    }else{
        return dateArray;
    }
}

module.exports=getDate;